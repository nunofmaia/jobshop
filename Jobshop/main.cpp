//
//  main.cpp
//  Jobshop
//
//  Created by Nuno Maia on 4/21/13.
//  Copyright (c) 2013 Nuno Maia. All rights reserved.
//

#include <iostream>
#include <limits>
#include <list>
#include <vector>

using namespace std;

struct Job;
struct Student;
struct NIL;

#define INFINITY numeric_limits<int>::max()

struct Vertex {
    int id;
    int distance;
    Vertex *pair;
    
    Vertex() : id(-1), distance(INFINITY), pair(NULL) {
        
    }
    
    Vertex(int id, Vertex* pair) : id(id), distance(INFINITY), pair(pair) {
        
    }
    
    ~Vertex() {
        delete pair;
    }
};

struct NIL : public Vertex {
    
    NIL() : Vertex() {
        
    }
    
    ~NIL() {
        delete pair;
    }
};

struct Job : public Vertex {
    
    Job() : Vertex() {
        
    }
    
    Job(int id, Vertex* pair) : Vertex(id, pair) {
        
    }
    
    ~Job() {
        delete pair;
    }
    
};

struct Student : public Vertex {
    vector<Job*> *jobs;
    
    Student() : Vertex(), jobs(new vector<Job*>()) {
        
    }
    
    Student(int id, Vertex* pair) : Vertex(id, pair), jobs(new vector<Job*>()) {
        
    }
    
    ~Student() {
        delete pair;
        delete jobs;
    }
    
};

class HopcroftKarp {
    
private:
    NIL *nil;
    vector<Student*> *students;
    vector<Job*> *jobs;
    
    bool BFS() {
        
        list<Vertex*> *queue = new list<Vertex*>();
        vector<Student*>::iterator st;
        for (st = students->begin(); st != students->end(); st++) {
            if ((*st)->pair == nil) {
                (*st)->distance = 0;
                queue->push_back(*st);
            } else {
                (*st)->distance = INFINITY;
            }
        }
        
        nil->distance = INFINITY;
        while (!queue->empty()) {
            Student *student = (Student*)queue->front();
            queue->pop_front();
            
            if (student->distance < nil->distance) {
                Vertex *pair;
                vector<Job*>::iterator jb;
                for (jb = student->jobs->begin(); jb != student->jobs->end(); jb++) {
                    pair = (*jb)->pair;
                    if (pair->distance == INFINITY) {
                        pair->distance = student->distance + 1;
                        queue->push_back(pair);
                    }
                }
                
            }
        }
        
        delete queue;
        
        return nil->distance != INFINITY;
    }
    
    bool DFS(Student *student) {
        
        if ((Vertex*)student != nil) {
            vector<Job*>::iterator job;
            for (job = student->jobs->begin(); job != student->jobs->end(); job++) {
                if ((*job)->pair->distance == student->distance + 1) {
                    if (DFS((Student*)(*job)->pair)) {
                        (*job)->pair = student;
                        student->pair = *job;
                        
                        return true;
                    }
                }
            }
            
            student->distance = INFINITY;
            
            return false;
        }
        
        return true;
    }
    
public:
    
    HopcroftKarp() : nil(new NIL()) {
        
    }
    
    ~HopcroftKarp() {
        delete nil;
        delete students;
        delete jobs;
    }
    
    
    void run() {
        
        int matching = 0;
        int n_students;
        int n_jobs;
        
        cin >> n_students >> n_jobs;
        
        students = new vector<Student*>(n_students);
        jobs = new vector<Job*>(n_jobs);
        
        
        for (int i = 0; i < n_students; i++) {
            
            Student *student = new Student(i, nil);
            int job_counter;
            
            cin >> job_counter;
            
            for (int j = 0; j < job_counter; j++) {
                int job;
                cin >> job;
                
                if (jobs->at(job) == NULL) {
                    jobs->at(job) = new Job(job, nil);
                }
                
                student->jobs->push_back(jobs->at(job));
                
            }
            
            students->at(i) = student;
        }
        
        while (BFS()) {
            vector<Student*>::iterator student;
            for (student = students->begin(); student != students->end(); student++) {
                if ((*student)->pair == nil) {
                    if (DFS(*student)) {
                        matching++;
                    }
                }
            }
        }
        
        cout << matching << endl;
    }
    
};

int main(int argc, const char * argv[])
{
    HopcroftKarp *hk = new HopcroftKarp();
    
    hk->run();
    
    delete hk;
}

